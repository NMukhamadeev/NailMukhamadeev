package yasmtp.mail;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import yasmtp.mail.utils.EmaiUtil;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import java.util.Properties;


public class SendMailTest {

    private final String fromMail = "Nmukhamadeev@yandex.ru";
    private final String password = "datatech008";
    private final String toMail = "Nmukhamadeev@yandex.ru";

    @BeforeClass
    public static void setup() {
        System.setProperty("webdriver.chrome.driver", "E:\\chromedriver.exe");
    }

    @Test
    public void sendMail() {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.yandex.ru");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Authenticator auth = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromMail, password);
            }
        };

        Session session = Session.getDefaultInstance(props, auth);
        System.out.println("Session created");
        EmaiUtil.sendEmail(session, toMail,"SSLEmail Testing Subject", "SSLEmail Testing Body");

    }


    @AfterClass
    public static void tearDown() {

    }

}
